#!/bin/sh
set -e

for i in $(seq 1 8); do
  width=$(($(sed -n 2p original.ppm |cut -d\  -f1)/$i))
  sed "2 s/^[0-9]*/$width/" original.ppm > "growing-$i.ppm"
  convert "growing-$i."{ppm,png}
done
