class faceObj{
  
  float [] vA = new float [3];
  float [] vB = new float [3];
  float [] vC = new float [3];
  
  faceObj(int a0, int a1, int a2, int b0, int b1, int b2, int c0, int c1, int c2 ){
    
    vA[0] = float(a0);
    vA[1] = float(a1);
    vA[2] = float(a2);
    
    vB[0] = float(b0);
    vB[1] = float(b1);
    vB[2] = float(b2);
    
    vC[0] = float(c0);
    vC[1] = float(c1);
    vC[2] = float(c2);
    
  }
  
  void display(){
    
    strokeWeight(1/SCALE);
    noStroke();
    fill(255);
     
    beginShape();
      vertex(vA[0], vA[1], vA[2]);
      vertex(vB[0], vB[1], vB[2]);
      vertex(vC[0], vC[1], vC[2]);
    endShape(CLOSE);
     
  }
 
}
/////////////////////////////////////////////////////////////

void initializeFaces(){
  
    PImage pixData;
    pixData = loadImage("data/pixGraph.png");
    pixData.loadPixels();
    println(pixData.pixels.length);

    for (int i=0; i<pixData.pixels.length; i+=9){
      if (pixData.pixels[i]!=0x00000000){
        
        int yVal = i/pixData.width;
        
        int aX = pixData.pixels[i];
        int aY = pixData.pixels[i+1];
        int aZ = pixData.pixels[i+2];
        
        int bX = pixData.pixels[i+3];
        int bY = pixData.pixels[i+4];
        int bZ = pixData.pixels[i+5];
        
        int cX = pixData.pixels[i+6];
        int cY = pixData.pixels[i+7];
        int cZ = pixData.pixels[i+8];
        
        face = new faceObj(aX, aY, aZ, bX, bY, bZ, cX, cY, cZ) ;
        
        slicedFaces.add(face);  
 
      }  
    }
    
    println("initialized");

}

