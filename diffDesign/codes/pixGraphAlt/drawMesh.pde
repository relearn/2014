///the mesh can be drawn using the original stl file
void drawMesh(){
  
  //gfx.origin(new Vec3D(),300);
  
  directionalLight(125, 125, 125, 0, 1, -1);
  ambientLight(75,75,75);
  strokeWeight(1/SCALE);
  stroke(0,20); 
  fill(255);
  //mesh.scale(stlScale);
  gfx.mesh(mesh,false,0);
  
}

///the mesh can be drawn from the simple table of faces
void drawCSV(){
  
  directionalLight(125, 125, 125, 0, 1, -1);
  ambientLight(75,75,75);
  strokeWeight(1/SCALE);
  stroke(0,20); 
  fill(255);
 
  for (int i =0; i< vTable.getRowCount(); i++){
    float[] colPosA = new float[3];
    float[] colPosB = new float[3];
    float[] colPosC = new float[3];
    
    colPosA[0]= vTable.getFloat(i, "x0");
    colPosA[1]= vTable.getFloat(i, "y0");
    colPosA[2]= vTable.getFloat(i, "z0");
      
    colPosB[0]= vTable.getFloat(i, "x1");
    colPosB[1]= vTable.getFloat(i, "y1");
    colPosB[2]= vTable.getFloat(i, "z1");
      
    colPosC[0]= vTable.getFloat(i, "x2");
    colPosC[1]= vTable.getFloat(i, "y2");
    colPosC[2]= vTable.getFloat(i, "z2");
   
    beginShape();
      vertex(colPosA[0], colPosA[1], colPosA[2]);
      vertex(colPosB[0], colPosB[1], colPosB[2]);
      vertex(colPosC[0], colPosC[1], colPosC[2]);
    endShape(CLOSE);
    
  }
}
  

