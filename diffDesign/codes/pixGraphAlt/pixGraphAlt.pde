TriangleMesh mesh;
ToxiclibsSupport gfx;

int screenH = 600;
int screenW = 1200;
int screenGUI = 0;

ArrayList<Face> facesList;
Table vTable;

faceObj face;

ArrayList<faceObj> slicedFaces;

void setup() {

 noLoop();
 size(screenW, screenH,OPENGL);

 //////////////////establish twitter connection
 //connectTwitter();
 //////////////////establish twitter connection
 
 slicedFaces = new ArrayList<faceObj>();
 
 loadStlData();
 getGraphSize();
 sortGraph();
 
 initializeFaces();

}

void draw() {
  
  background(0);
  
//  if(tweetImage==true){
//    println("tweeting...");
//    tweetNewImage();
//    tweetImage=false;
//  }

//  pushMatrix();
//    translate((width/6)*2,height/2,0);
//    rotateY(ROTX);
//    rotateX(ROTY);
//    scale(SCALE);
//    drawMesh();  
//  popMatrix();
    
  pushMatrix();
    translate((width/6)*3,height/2,0);
    rotateY(ROTX);
    rotateX(ROTY);
    scale(SCALE);
    
    directionalLight(125, 125, 125, 0, 1, -1);
    ambientLight(75,75,75);
    for (int i=0; i<slicedFaces.size(); i++){ 
      faceObj tempFace = slicedFaces.get(i);
      tempFace.display();   
    }
    noFill();
    stroke(255);
    box(colRange,colRange,colRange);
    noLights();
    
   popMatrix();
  
   image(pg, 0, 0);
  
//  if(makeTweetImage==true){
//    makeTweetImage();
//    makeTweetImage=false;
//    tweetImage=true;
//  }

}
