////this is a class for the buttons at the top of the screen that selects the 'active' image/ model to display
class buttonSlider{

  int index;
  int posX=0;
  int posY=0;
  int dimX=(screenW);
  int dimY=guiDim;
  int activeVal=0;
  int activeIndex=0;
  String activeFile;
  int sepWidth=0;
  
  buttonSlider(){
    activeFile = pixList.get(activeIndex);
  }
  
  void update(){
    if ((mouseY<screenGUI)&&(mouseX>posX)&&(mouseX<posX+dimX)){
      activeVal = mouseX-posX;
      activeIndex = int(map(activeVal, 0, screenW, 0, pixList.size()));
      activeFile = pixList.get(activeIndex);
      println(activeFile);
    }
  }
  
  void display(){
         
    fill(255);
    strokeWeight(1);
    stroke(0);
    rect(posX,posY,dimX, dimY);
    
    for (int i=0; i<pixList.size(); i++){
      if (activeIndex>=i){
        fill(255);
      } else {
        fill(50);
      }
      strokeWeight(1);
      stroke(0);
      rect(posX+(i*(screenW/pixList.size())),posY,dimX, dimY);
    }
    
    fill(255);
    textAlign(LEFT, TOP);
    textSize(guiDim/2);
    text(activeFile, posX+guiDim, height-screenGUI/2); 
  }
  
}

//////////////////////////////////////////////////////////////////
void initializeSliders(){
  buttonSlide = new buttonSlider();
}


