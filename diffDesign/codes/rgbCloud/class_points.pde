////this is a class for the point objects to make the cloud
class pointObject{
  
  int rVal;
  int gVal;
  int bVal;
  boolean active;
  
  pointObject(int r, int g, int b){
    rVal = r;
    gVal = g;
    bVal = b;
  }
  
  void display(){
    strokeWeight(4);
    stroke(rVal, gVal, bVal,100);  
    point(rVal-128, gVal-128, bVal-128); 
  }
 
}
////////////////////////////////////////////////////////

void drawPoints(){
  for (int i=0; i<strPix.size(); i++){
    String tempPix = strPix.get(i);
    int[] pix = int(split(tempPix, ","));
    pointObject pObjTemp = new pointObject(pix[0],pix[1],pix[2]);
    pObjTemp.display();
  }
}


