float ROTX = 0;                        
float ROTY = 0;
float SCALE = 1;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////controls the rotation and zoom of the display
void mouseDragged() {
   
   if (mouseButton == LEFT) {
     ROTX += (mouseX - pmouseX) * 0.01;
     ROTY -= (mouseY - pmouseY) * 0.01;
   }
   if (mouseButton == RIGHT) {
     if (mouseY>pmouseY){
        SCALE = SCALE+0.1;
     } else {
        SCALE = SCALE-0.1;
     }
     if (SCALE<0.2){ SCALE=0.2;}
     if (SCALE>=10){ SCALE=10;}
   }
   redraw();

}

////loads a new image if a button is pressed
void mouseReleased(){
   if (mouseY<guiDim){
     buttonSlide.update();
     getNewPixels=true;
     redraw();  
   }
}

/////keyboard controls

void keyReleased() {
  /////hit space to tweet an image
  if ( keyCode == 32) {
    makeTweetImage=true;
    redraw();
  } 
  /////hit enter to search twitter
  if ( key == ENTER) {
    searchTwitter=true;
    redraw();
  } 
  /////begin typing to make a search term
  if ( keyCode != 32 && key != ENTER) {
    int searchTermLength = searchTerm.length();
   
    if (key == BACKSPACE) {
      if (searchTermLength>1){
        String tempSearchTerm = searchTerm.substring(0, searchTermLength-1);
        searchTerm = trim(tempSearchTerm);
        println(searchTerm);
      }
      
    } else {
      searchTerm+=key;
      println(searchTerm);
    }
    
    buttonTextBox.update();

    showSearch=true;
    redraw();
    
  }
}








