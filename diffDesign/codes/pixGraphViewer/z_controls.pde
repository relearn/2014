float ROTX = 0;                        
float ROTY = 0;
float SCALE = 0.0001;

boolean displaySlices = true;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////control the rotation and scale display of the model using the mouse
void mouseDragged() {
   
   if ((mouseY>screenGUI/2)&&(mouseY<screenH+(screenGUI/2))){
     if (mouseButton == LEFT) {
       ROTX += (mouseX - pmouseX) * 0.01;
       ROTY -= (mouseY - pmouseY) * 0.01;
     }
     if (mouseButton == RIGHT) {
       if (mouseY>pmouseY){
          SCALE = SCALE+0.00001;
       } else {
          SCALE = SCALE-0.00001;
       }
       if (SCALE<0.00001){ SCALE=0.00001;}
       if (SCALE>=0.001){ SCALE=0.001;}
     }
     redraw();
   }
   
}

////press spacebar to send a tweet of the pixGrpah
void keyReleased() {
  if ( keyCode == 32) {
    makeTweetImage=true;
  }
  
  if ( key == 'd' || key =='D') {
    if (displaySlices==false){
      displaySlices=true;
    } else {
      displaySlices=false;
    }
    redraw();
  }
}



