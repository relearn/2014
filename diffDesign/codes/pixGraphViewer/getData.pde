float minX = 16777216;
float maxX = -16777216;
float minY = 16777216;
float maxY = -16777216;
float minZ = 16777216;
float maxZ = -16777216;

int colRangeMin = -16777216;
int colRangeMax = -1;
int colRange = colRangeMax-colRangeMin;

int maxFactor = 3;
float minScale;
float maxScale;

float xRange;
float yRange;
float zRange;

float maxZslice = 16777216;

void initializeFaces(){
  
    pixData = loadImage("data/pixGraph.png");
    
    pixData.loadPixels();
    println(pixData.pixels.length);

    for (int i=0; i<pixData.pixels.length; i+=9){
      if (pixData.pixels[i]!=0x00000000){
        
        int rowVal = i/pixData.width;
        //println(rowVal);
        
        int aX = pixData.pixels[i];
        int aY = pixData.pixels[i+1];
        int aZ = pixData.pixels[i+2];
        
        int bX = pixData.pixels[i+3];
        int bY = pixData.pixels[i+4];
        int bZ = pixData.pixels[i+5];
        
        int cX = pixData.pixels[i+6];
        int cY = pixData.pixels[i+7];
        int cZ = pixData.pixels[i+8];
        
        face = new faceObj(aX, aY, aZ, bX, bY, bZ, cX, cY, cZ, rowVal) ;
        
        slicedFaces.add(face);  
        
        float tempX;
        tempX = (aX);
        if (tempX<minX){ minX=tempX;}
        if (tempX>maxX){ maxX=tempX;}
        tempX = (bX);
        if (tempX<minX){ minX=tempX;}
        if (tempX>maxX){ maxX=tempX;}
        tempX = (cX);
        if (tempX<minX){ minX=tempX;}
        if (tempX>maxX){ maxX=tempX;}
        
        float tempY;
        tempY = (aY);
        if (tempY<minY){ minY=tempY;}
        if (tempY>maxY){ maxY=tempY;}
        tempY = (bY);
        if (tempY<minY){ minY=tempY;}
        if (tempY>maxY){ maxY=tempY;}
        tempY = (cY);
        if (tempY<minY){ minY=tempY;}
        if (tempY>maxY){ maxY=tempY;}
        
        float tempZ;
        tempZ = (aZ);
        if (tempZ<minZ){ minZ=tempZ;}
        if (tempZ>maxZ){ maxZ=tempZ;}
        tempZ = (bZ);
        if (tempZ<minZ){ minZ=tempZ;}
        if (tempZ>maxZ){ maxZ=tempZ;}
        tempZ = (cZ);
        if (tempZ<minZ){ minZ=tempZ;}
        if (tempZ>maxZ){ maxZ=tempZ;}
 
      }  
    }
    
    xRange = maxX-minX;
    yRange = maxY-minY;
    zRange = maxZ-minZ;
    
    println(xRange + " <> " +yRange + " <> " + zRange);
    
    println("xRange: " + minX + " <> " + maxX);
    println("yRange: " + minY + " <> " + maxY);
    println("zRange: " + minZ + " <> " + maxZ);
    
    println("loaded");  
  
    if (xRange>yRange) {
      if (xRange>zRange) {
        maxFactor=0;
        minScale = minX;  
        maxScale = maxX; 
      } else {
        maxFactor=2;
        minScale = minZ;  
        maxScale = maxZ; 
      }    
    } else { 
      if (yRange>zRange) {
        maxFactor=1; 
        minScale = minY;  
        maxScale = maxY; 
      } else {
        maxFactor=2; 
        minScale = minZ;  
        maxScale = maxZ;  
      }    
    }  
    
    

}
