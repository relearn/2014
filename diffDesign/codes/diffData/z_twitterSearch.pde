boolean searchTwitter = false;
boolean showSearch = false;

String searchTerm = "#textSearch";
String searchSince = "2014-07-07";
String searchUntil = "2014-07-11";

int maxTweets = 10;
int numTweets = 1;

Table tweetTable;

void twitterSearches(){
  
  tweetTable = new Table();
  
  Query query = new Query(searchTerm);
//  query.since(searchSince);
//  query.since(searchUntil);
  query.setCount(maxTweets);
  query.resultType("ALL");
  
  tweetTable.addColumn("@");
  tweetTable.addColumn("Latitude");
  tweetTable.addColumn("Longitude");
  tweetTable.addColumn("Time");
  tweetTable.addColumn("Tweet");
  tweetTable.addColumn("Media");
  
  try {

    QueryResult result = twitterSearch.search(query);
    int ind = 0;
    
    ////return tweets from first page of the search   
    for (Status status : result.getTweets()) {
      String Lat = "";
      String Long = " ";
      String ID = "@" + status.getUser().getScreenName();
      
      if (status.getGeoLocation() != null){
        Lat = " " + status.getGeoLocation().getLatitude();
        Long = " " + status.getGeoLocation().getLongitude();
        Lat = trim(Lat); 
        Long = trim(Long); 
      }
      String Time = "Time: " + status.getCreatedAt();
      String Twt = status.getText();
      String hTag = "";


      TableRow newRow = tweetTable.addRow();
      newRow.setString("@",ID); 
      newRow.setString("Latitude", Lat);
      newRow.setString("Longitude",Long);
      newRow.setString("Time",Time);
      newRow.setString("Tweet",Twt);
      
      println("Location: " + status.getGeoLocation());
      println("Time: " + status.getCreatedAt());
      println("@" + status.getUser().getScreenName() + ":" + status.getText());
      
      /////get hashtags from tweets     
//      for (HashtagEntity hashtags : status.getHashtagEntities()) {
//        hTag = hTag +"#" + hashtags.getText() + ", ";
//        //println(hTag);
//      }
//      newRow.setString("Tags",hTag);

      int tagCount=0;
      for (HashtagEntity hashtags : status.getHashtagEntities()) {
        hTag = hashtags.getText();
        newRow.setString("Tags"+tagCount,hTag);
        tagCount++;
        //println(hTag);
      }
      
      /////get images from tweets
      String imgUrl = null;
      String imgPage = null;
      
      for (MediaEntity mediaEntity : status.getMediaEntities()) {
        imgUrl = mediaEntity.getMediaURL();
        //println(imgUrl);           
        //println(ind);  
        byte[] imgBytes = loadBytes(imgUrl);
        saveBytes("data/" + ind + ".jpg", imgBytes);
        newRow.setInt("Media",ind);
      }

    ind = ind+1;

    }
    
////return tweets from additinal pages of the search
//    for (int i = 1; i<(numTweets/maxTweets); i++){    
//      if(result.hasNext()){
//        //println("TRUE");
//        query = result.nextQuery();
//        result = twitterSearch.search(query);
//        for (Status status : result.getTweets()) {
//          String Lat = "";
//          String Long = " ";
//          String ID = "@" + status.getUser().getScreenName();
//          if (status.getGeoLocation() != null){
//            Lat = " " + status.getGeoLocation().getLatitude();
//            Long = " " + status.getGeoLocation().getLongitude();
//            Lat = trim(Lat); 
//            Long = trim(Long); 
//          }
//          String Time = "Time: " + status.getCreatedAt();
//          String Twt = status.getText();
//          String hTag = "";
//          
//          TableRow newRow = tweetTable.addRow();
//          newRow.setString("@",ID); 
//          newRow.setString("Latitude", Lat);
//          newRow.setString("Longitude",Long);
//          newRow.setString("Time",Time);
//          newRow.setString("Tweet",Twt);
//          
//          println("Location: " + status.getGeoLocation());
//          println("Time: " + status.getCreatedAt());
//          println("@" + status.getUser().getScreenName() + ":" + status.getText());
//          
//          /////get hashtags from tweets     
//          int tagCount=0;
//          for (HashtagEntity hashtags : status.getHashtagEntities()) {
//            hTag = hashtags.getText();
//            newRow.setString("Tags"+tagCount,hTag);
//            tagCount++;
//            //println(hTag);
//          }
//           
//          /////get images from tweets
//          String imgUrl = null;
//          String imgPage = null;
//          
//          for (MediaEntity mediaEntity : status.getMediaEntities()) {
//            imgUrl = mediaEntity.getMediaURL();
//            //println(imgUrl);           
//            //println(ind);  
//            byte[] imgBytes = loadBytes(imgUrl);
//            saveBytes("data/" + ind + ".jpg", imgBytes);
//            newRow.setInt("Media",ind);
//          }
//    
//          ind = ind+1;
//          
//        }
//      }
//    }
////return tweets from additinal pages of the search

  }catch(TwitterException e) {
      println("noTweets");
  }
  
  saveTable(tweetTable, "data/searches/tweetData" + searchTerm + ".csv");
  println("csv exported....");
}





  
