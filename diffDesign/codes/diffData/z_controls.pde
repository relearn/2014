float ROTX = 0;                        
float ROTY = 0;
float SCALE = 1;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////controls the rotation and zoom of the 3D model
void mouseDragged() {
   
   if ((mouseY>screenGUI/2)&&(mouseY<screenH+(screenGUI/2))){
     if (mouseButton == LEFT) {
       ROTX += (mouseX - pmouseX) * 0.01;
       ROTY -= (mouseY - pmouseY) * 0.01;
     }
     if (mouseButton == RIGHT) {
       if (mouseY>pmouseY){
          SCALE = SCALE+0.1;
       } else {
          SCALE = SCALE-0.1;
       }
       if (SCALE<0.2){ SCALE=0.2;}
       if (SCALE>=10){ SCALE=10;}
     }
     redraw();
   }
   
}

////keyboard controls
void keyReleased() {
  ////press spacebar to send a tweet
  if ( keyCode == 32) {
    makeTweetImage=true;
    redraw();
  } 
  ////press enter to search twitter for new image files
  if ( key == ENTER) {
    searchTwitter=true;
    redraw();
  } 
  ////begin typing to create a search term for twitter
  if ( keyCode != 32 && key != ENTER) {
    int searchTermLength = searchTerm.length();
   
    if (key == BACKSPACE) {
      if (searchTermLength>1){
        String tempSearchTerm = searchTerm.substring(0, searchTermLength-1);
        searchTerm = trim(tempSearchTerm);
        println(searchTerm);
      }
      
    } else {
      searchTerm+=key;
      println(searchTerm);
    }
    
      buttonTextBox.update();
    showSearch=true;
    redraw();
    
  }
}

////update files according to button clicks
void mouseReleased(){
   if (mouseY>screenH + (screenGUI/2)){
     for (int i=0; i<3; i++){
       buttonClick[i].update();
     }
     redraw();  
   }
   
   if (mouseY<screenGUI/2){
     for (int i=0; i<3; i++){
       buttonSlide[i].update();
     }
     redraw();  
   }
}





