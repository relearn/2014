void drawModel(){
  
  directionalLight(125, 125, 125, 0, 1, -1);
  ambientLight(200,200,200);
    
  pushMatrix();
    translate(screenW/2, (screenH/4)*3);
    rotateY(ROTX);
    rotateX(ROTY);
    scale(SCALE);
    drawGeom();
    drawAxis();
  popMatrix();
  
  noLights();
  
}

////draw the geometry - specify which values are controlled by the sliders
void drawGeom(){
  
  buttonSlide[0].name = "arraySize";
  buttonSlide[1].name = "spacing";
  buttonSlide[2].name = "rotation";
  buttonSlide[3].name = "width";
  buttonSlide[4].name = "lenght";
  
  strokeWeight(1/SCALE);
  stroke(255); 
  noFill();
  
  int rotVal=0;
  int transVal=0;
  
  for (int i=0; i<buttonSlide[0].val; i++){
    rotVal+=buttonSlide[2].val;
    pushMatrix();
      translate(0,0,i*buttonSlide[1].val);
      rotate(rotVal);
      rectMode(CENTER);
      rect(0, 0,buttonSlide[4].val*2, buttonSlide[5].val*2);
    popMatrix();
  }
  
}

void drawAxis(){
  stroke(255,0,0); 
  line(0,0,0,0,0,100);
  stroke(0,255,0); 
  line(0,0,0,0,100,0);
  stroke(0,0,255); 
  line(0,0,0,100,0,0); 
}



