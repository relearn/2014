import nervoussystem.obj.*;
import processing.dxf.*;

boolean record;

PImage pixData;
String fileName = "data/growing-5.png";
pointObject pObj;

ArrayList<pointObject> pObjs;

void setup(){
  
  noLoop();
  size(1200, 800, OPENGL);
  background(0);
  
  pObjs = new ArrayList<pointObject>();
  getPix();
  
}

void draw(){
  
  if (record) {
    beginRaw("nervoussystem.obj.OBJExport", "newFile.obj");
    //beginRaw(DXF, "output.dxf");
  }
  
  background(0);
  
  pushMatrix();
    translate(width/2,height/2,0);
    rotateY(ROTX);
    rotateX(ROTY);
    scale(SCALE);
    drawPoints();
  popMatrix();
  
  if (record) {
    endRaw();
    record = false;
    println("recorded");
  }
  
  saveFrame("image-######.png");
      
}

