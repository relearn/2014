float ROTX = 0;                        
float ROTY = 0;
float SCALE = 0.7;

boolean drawR=true;
boolean drawB=true;
boolean drawG=true;
boolean drawI=true;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////control the rotation and scale display of the model using the mouse
void mouseDragged() {
   
     if (mouseButton == LEFT) {
       ROTX += (mouseX - pmouseX) * 0.01;
       ROTY -= (mouseY - pmouseY) * 0.01;
     }
     if (mouseButton == RIGHT) {
       if (mouseY>pmouseY){
          SCALE = SCALE+0.1;
       } else {
          SCALE = SCALE-0.1;
       }
//       //if (SCALE<0.1){ SCALE=0.1;}
//       //if (SCALE>=10){ SCALE=10;}
     }
     redraw();
   
}

void keyReleased(){
  
  if (key == 'r' || key== 'R'){
    if(drawR==false){
      drawR=true;
    } else{
      drawR=false;
    }
    redraw();
  }
  if (key == 'g' || key== 'G'){
    if(drawG==false){
      drawG=true;
    } else{
      drawG=false;
    }
    redraw();
  }
  if (key == 'b' || key== 'B'){
    if(drawB==false){
      drawB=true;
    } else{
      drawB=false;
    }
    redraw();
  }
  if (key == 'i' || key== 'I'){
    if(drawI==false){
      drawI=true;
    } else{
      drawI=false;
    }
    redraw();
  }
  
  if (key == 'r'||key =='R') {
    record = true;
    redraw();
  }
  
  
}




