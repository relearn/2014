A diff(erent) kind of design
=============================

How does code make space? How does make space code? Starting from an open, networked, 'versioning' tool for architectural design, we experimented with transformations between 2D and 3D space. We used 'versioning' as a way of disrupting and remaking conventional 3D design work flows; creating 3D models based on 2D transformations and vice versa. Intercepting streams of computational design data we dynamically explored the pasts, presents and futures of our digital designs.

* Prepared by: [Phil Langley](http://tinyurl.com/phiLangley)
* With contributions from: Gemma Sutton, Alexia de Visscher, Maria Sole Bravo, Pepa Ivanova, Phil Langley, Julien Deswaef, Femke Snelting, Freya Vandenboom, Antonio Roberts, Daphne Bom + New Babylon

2D-3D Explorations
------------------


GIMP interface
--------------
Thinking about possible interfaces to GIMP that allow users to control 3D modelling in a 2D space.


Datamodel
---------
By carefully analysing the datamodel of the *diff(erent) kind of design* software proposal, we decided to change the datamodel, so that height of the datapresentation would map to the height of the 3D-model. This made it possible to edit the models both in 2D and 3D space.

Storing 3D model information into a 2D image using colored pixels as representation of x,y,z coordinates.  
Each line of pixel is a "slice" of the 3D model, starting from the "top".  

In the first days, the main question was how to use 2D filters and/or 2D tools to transform/edit/modify a 3D model in a "coherent" or "somewhat predictible" way.  
So we went through an iteration of code and data model (2D representation) changes to try to achieve an acceptable solution to this experiment.

![data-model](/diffDesign/codes/pixGraphAlt/data/pixGraph.png)

A Processing sketch to understand the relation between colors and integers: 
[color-to-integer](/diffDesign/codes/color_to_integer/)

[Screenshots from 2D tools explorations](/diffDesign/2Dto3D/PrintScreens/)


Experiments with elevated images
--------------------------------
Starting from physical drawing images, Pepa experimented with generating 3D models. The final result will be a video.

[3DimageGraph](/diffDesign/codes/sketch_3DimageGraph/)


Doorlabyrinth
-------------
The objective is to navigate a catalogue of images in diff(erent) way. An image ir loaded and its pixel lightness is used to create a 3D geometry. From there, by rotating the 3D model in certain directions, a new image will be loaded and the 3D geometry will be recreated accordingly.

[doorLabyrinth](/diffDesign/codes/doorLabyrinth)

