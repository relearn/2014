import math

from chiplotle.geometry.transforms.rotate import rotate
from chiplotle.geometry.transforms.offset import offset
from chiplotle.geometry.core.path import Path
from chiplotle.geometry.core.coordinate import Coordinate
from chiplotle.geometry.core.coordinatearray import CoordinateArray
from chiplotle.tools import plottertools
from chiplotle.hpgl import commands
from chiplotle import io
from chiplotle import instantiate_plotters

# Draws a full line
def full (i, angle, length):
    # We only draw the 'odd' lines
    if (i % 2 == 1):
        return [
            Path([Coordinate(0,0), Coordinate(length, 0)])
        ]
    else:
        return []

# Draws half a line.
def half (i, angle, length):
    # We only draw the 'even' lines
    if (i % 2 == 0):
        quarter_length = length * .25
        
        return [
            Path([Coordinate(0,0), Coordinate(quarter_length, 0)]),
            Path([Coordinate(length - quarter_length, 0), Coordinate(length, 0)])
        ]
    else:
        return []

def dashed (i, angle, length):
    dashlength = .05 * length
    gaplength = .05 * length
    
    steplength = dashlength + gaplength
    dashes = int(length  / steplength)
    
    return [Path([Coordinate(i * steplength,0), Coordinate(i * steplength + dashlength, 0)]) for i in range(dashes)]

layers = [dashed, half, full]


virtual = "virtual"
hardware = "hardware"

mode = virtual

plotter = plottertools.instantiate_virtual_plotter(type="HP7550A") if mode == virtual else instantiate_plotters()[0]

margins = plotter.margins.soft
width = margins.right - margins.left
height = margins.top - margins.bottom

length = 100 * 40
steps = 20

plotter.write(commands.SP(1))
plotter.write(commands.VS(15))

for i in range(steps):
    a = (float(i) / float(steps)) * math.pi
    for layer in layers:
        for path in layer(i, a, length):
            offset(path, Coordinate(-length * .5, 0))
            rotate(path, a)
            offset(path, Coordinate(width * .5, height * .5))
            plotter.write(path)
    
plotter.write(commands.SP(0))

if mode == virtual:
    io.view (plotter)