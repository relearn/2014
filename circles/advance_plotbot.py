import irc.bot
import chiplotle
from random import randint


class PlotBot(irc.bot.SingleServerIRCBot):
    def __init__(self, plotter, channel, nickname, server, port=6667):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.plotter = plotter
        self.channel = channel

    def on_welcome(self, c, e):
        print "Bot connected"
        c.join(self.channel,)

    def on_privmsg(self, c, e):
        self.on_pubmsg(c, e)

    def on_pubmsg(self, c, e):
        msg = e.arguments[0].decode("utf-8")
        #this count the length of a message and draws a circle accordingly
        #msg = e.arguments[0]
        if msg.startswith("!"):
            msg = msg.lstrip("!")
            self.plotter.write(str(msg))
        else:
            count = len(msg)
            wordcount = count*300
            c='CI{0}'.format(wordcount)
            self.plotter.write(str(c))
        if msg.find("?")>=0:
            c.privmsg(self.channel, "Resource: http://mercator.elte.hu/~saman/hu/okt/HPGL.pdf")   
        

    def on_join(self, c, e):
        p=randint(1,4)
        #this  adds the random pen select on login of new user
        self.plotter.write('SP'+str(p))
        c.privmsg(self.channel, "Welcome to the Plotbot IRC channel!")
        c.privmsg(self.channel, "To operate the plotter input your specific commands now.")
        
        

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='BashBot')
    parser.add_argument('--host', default="localhost", help='host')
    parser.add_argument('--port', type=int, default=6667, help='port')
    parser.add_argument('channel', help='channel to join')
    parser.add_argument('nickname', help='bot nickname')
    args = parser.parse_args()
    if not args.channel.startswith("#"):
        args.channel = "#"+args.channel

    plotter = chiplotle.instantiate_plotters()[0]
    bot = PlotBot(plotter, args.channel, args.nickname, args.host, args.port)
    bot.start()

