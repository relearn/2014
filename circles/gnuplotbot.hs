import Network
import System.IO
import Text.Printf
import Data.List (stripPrefix)

server = "irc.freenode.org"
port   = 6667
nick   = "gnuplotbot"
 
connect :: String -> IO Handle
connect = do
    h <- connectTo server (PortNumber (fromIntegral port))
    hSetBuffering h NoBuffering
    write h "NICK" nick
    write h "USER" $ "tlevine-" ++ nick
    write h "JOIN" "gnuplotbot"
    write h "JOIN" "plotbot"
    return h
 
write :: Handle -> String -> IO ()
write h s t = do
    hPrintf h "%s %s\r\n" s t
    printf    "> %s %s\n" s t
 
listen :: Handle -> Handle -> IO ()
listen handle = forever $ do
    command <- hGetLine handle
    write handle "PRIVMSG" "plotbot :" ++ command
  where
    forever a = do a; forever a

main = do
  listen connect
