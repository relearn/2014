import math

from chiplotle.geometry.core.path import Path
from chiplotle.geometry.transforms.offset import offset
from chiplotle.geometry.core.coordinate import Coordinate
from chiplotle.geometry.core.coordinatearray import CoordinateArray
from chiplotle.tools import plottertools
from chiplotle.hpgl import commands
from chiplotle import io
from chiplotle import instantiate_plotters

plotter = instantiate_plotters()[0]

margins = plotter.margins.soft
width = margins.right - margins.left
height = margins.top - margins.bottom
#plotter = plottertools.instantiate_virtual_plotter(type="HP7550A")


r = 2000

plotter.write(commands.SP(1))
plotter.write(commands.VS(4))

for steps in range(1, 40):
    circlePoints = CoordinateArray()
    for i in range(steps + 1):
        a = (float(i) / float(steps)) * math.pi * 2
        c = Coordinate(math.cos(a), math.sin(a)) * r
        c = Coordinate(c.x, c.y)
        circlePoints.append(c)
        
    circle = Path(circlePoints)
    offset(circle, Coordinate(width * .5, height * .5))
    plotter.write(circle)

#print circlePoints

plotter.write(commands.SP(0))

#io.view (plotter)