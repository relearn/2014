import irc.bot
from random import randint

from polar.polar import spiral

class ReportPlotBot(irc.bot.SingleServerIRCBot):
    def __init__(self, observechannel, plotchannel, nickname, server, port=6667, plotter = None):
        irc.bot.SingleServerIRCBot.__init__(self, [(server, port)], nickname, nickname)
        self.observechannel = observechannel
        self.plotchannel = plotchannel
        self.plotter = plotter

    def on_welcome(self, c, e):
        print "Bot connected"
        c.join(self.observechannel)
        c.join(self.plotchannel)
        c.privmsg(self.observechannel, "Your activity is now being plotted in circles!")

    def on_privmsg(self, c, e):
        self.on_pubmsg(c, e)

    def on_pubmsg(self, c, e):
        if e.target == self.observechannel:
            #this change the precision of the circle
            NbUsers = len(self.channels[e.target].users())
            rng=randint(1,6)
            #rng=1+(randint(0,100) * .01)
            angle=360/(NbUsers/rng)
            #choose a random position
            pX=randint(-24050,24050)
            pY=randint(-17975,17975)
            radius = 5000
            if e.target == self.observechannel:
                msg = e.arguments[0]
                NbWords = len(msg.split())
                d=50
                cl=spiral(pX - radius,pY - radius,pX + radius,pY + radius,30,NbWords,angle)
               #for x in range(0,NbWords):
               #    cl='PA'+str(pX)+','+str(pY)+';CI{0}'.format((d*x)+d)+','+str(angle)
               #    print cl  

                if self.plotter == None:
                    c.privmsg(self.plotchannel,str(cl))
                else:
                    self.plotter.write(cl)
                
                posN=(NbWords*d)+20
                pos = 'PR{0},{0}'.format(posN)
                c.privmsg(self.plotchannel, 'DT*,1;SI0.15,0.2;{0};LB{1}*'.format(pos,e.source.nick))

    #def on_join(self, c, e):
        #if e.target == self.observechannel:
            #p=randint(1,4)
            #this  adds the random pen select on login of new user
            #c.privmsg(self.plotchannel, 'SP{0}'.format(p))
        
        

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='BashBot')
    parser.add_argument('--host', default="localhost", help='host')
    parser.add_argument('--port', type=int, default=6667, help='port')
    parser.add_argument('observechannel', help='channel to join')
    parser.add_argument('plotchannel', help='channel to join')
    parser.add_argument('nickname', help='bot nickname')
    args = parser.parse_args()
    
    if not args.observechannel.startswith("#"):
        args.observechannel = "#"+args.observechannel

    if not args.plotchannel.startswith("#"):
        args.plotchannel = "#"+args.plotchannel


    bot = ReportPlotBot(args.observechannel, args.plotchannel, args.nickname, args.host, args.port)
    bot.start()

