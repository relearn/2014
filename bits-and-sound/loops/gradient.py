# Generates a gradient image in the ppm format
# Usage:
#     python gradient.py > myimage.ppm

print('P3')
print('51 5')
print('255')

for value in range(255):
    print('0 0 ' + str(value))
