# Generates a gradient image in the ppm format
# Usage:
#     python gradient.py > myimage.ppm
from random import randint

print('P3')
print('50 50')
print('255')

for value in range(255):
    print('0' + " " + str(value) + " " + str(value))
    print(str(value)+ " " + '0' + " " + str(value))
    print(str(value)+ " " + '0' + " " + '255')
    print('23 00 255')
for value in range(255):
    print('0' + " " + str(value) + " " + str(value))
    print(str(value)+ " " + '0' + " " + str(value))
    print('0' + " " + '0' + " " + '220')
    print('12 200 55')
for value in range(255):      
    print (str(value)+' 0 ' + str(value))       
    print ('0 ' + str(value) + ' ' + str(value))      
    print ('0 ' + str(value) + ' ' + str(value))      
    print ('0 ' + str(value) + ' ' + str(value))      
    print (str(value) + ' ' + str(value) + ' 0 ')             