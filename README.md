Portable Relearn Archive 2014
=============================

TO EDIT THESE PAGES
--------------
The pages of this (local) archive come from one of two sources:

* the local etherpad
* directy in the git repository

### To edit pages via the etherpad

* Go to the [local etherpad](http://192.168.1.222:9001/p/start) start page.
* Use double-brackets to link to new pages.
* Pages linked from the start page are published nightly to [the git repository](/etherpad_archive/)

### To edit pages directly via git

To edit / add pages or other kinds of files, checkout the relearn repository:

    git clone relearn@192.168.1.222:git/relearn.git

or if you've added relearn to your /etc/hosts:

    git clone relearn@relearn:git/relearn.git

From that point on, use the cd command to "get inside" the relearn repository folder:

    cd relearn

and add things with:

    git add mychangedfile
    git commit -m "My message"

and synchronize with:

    git pull
    git push

Links
-----------------
* [local etherpad](http://192.168.1.222:9001/p/start) (on port 9001)
* local IRC sever: use an IRC client (like Konversation, Colloquoy (OS X), mIRC (Windows)) to connect to host 192.168.1.222. Join #relearn or #2084.
* git

    git clone relearn@192.168.1.222:git/relearn.git

* [files](/share): You can use ssh (relearn/relearn) to upload files directly to the webserver (use this option for large files that don't need to be added to the git repo)

Photos
--------------

[![][group-photo]][photo-gallery]
[![][bot-making-2084]][photo-gallery]
[![][syncam]][photo-gallery]

[group-photo]: /photos/var/thumbs/Sunday/P7065177.JPG?m=1404757486 "Group Relearn Photo 2014"
[bot-making-2084]: /photos/var/thumbs/Tuesday/P7085200.JPG?m=1404983149 "Bot making 2084"
[syncam]: /photos/var/thumbs/Tuesday/P7085222.JPG?m=1404983306 "Syncam"
[photo-gallery]: /photos/

* [Photo Gallery](/photos): Photo gallery -- LOGIN with: relearn/relearn to add photos



A FEMINIST NET/WORK HOW-TO
-----------------------------

* Setup local server(s) & make our own network infrastructure, rather than "fixing the problems of internet access" by paying for increased bandwidth, 
* Prefer "read/write" networks to those that "just work" ,
* Prefer pocket servers to those in the clouds,
* Embrace a diversity of network topologies and different scales (machine to machine, local nodes, institutional infrastructure) and consider the implications of working with each,
 rather than a Web 2.0 model where resources must be uploaded onto "the 24/7 Internet (tm)" in order to share them,
* Invite users to look critically at the implications of *any* infrastructural decisions, rather than imagining utopic and/or "killer" solutions, 
* Make that which is normally hidden and invisible (tending to surveillance), explicit and shared (as a gesture of collective authorship), for instance:
    instead of caching web resources (silently), we imagine services to archive pages and share them locally as networked cookbooks,
    rather than logging IRC conversations on a server or database accessible only by administrators, we imagine (local) logs available for reading / editing by participants and published conditionally.

The provided network is a starting point; during summer school the network topology can be tweaked, changed, extended.
To this end, participants are encouraged to bring network devices (pirate box / openwrt routers / pi's / olimex / other obscure objects with ethernet ports).


