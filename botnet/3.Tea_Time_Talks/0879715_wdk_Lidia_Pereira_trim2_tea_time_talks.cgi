#!/usr/bin/env python
#-*- coding:utf-8 -*-
import cgi, urllib
import cgitb; cgitb.enable()
import urllib2, urlparse
from urlparse import urlparse
import html5lib
import random

form = cgi.FieldStorage()

q = form.getvalue("q","").strip().lower()
sessions = open("TeaTimeTalk/sessions.txt")
def id():
    lins = sessions.readlines()
    sesson = ""
    for line in lins:
        sesson = line
    return sesson

sessao = str(id())
reply = form.getvalue("reply","").strip().lower()
save = form.getvalue("save")
armazem = open("TeaTimeTalk/armazem.txt","a")
april = open("rabo.txt")
lines = april.readlines()

belezaFinal = open("TeaTimeTalk/"+sessao+".txt","a")
print "Content-Type: text/html"
print 
print """
    <!DOCTYPE html>
    <html>
    <head>
    <meta charset = utf-8 >
    <title>Tea Time Talks</title>
    <link href='http://fonts.googleapis.com/css?family=Crimson+Text' rel='stylesheet' type='text/css'>
    <link rel='stylesheet' type='text/css' href='/teatime.css'>  
    </head>
    <body onload='document.getElementById("reply").focus()'>
    <a href = 'frontpage.cgi'><img src = '/teatimebanner.png' class= 'banner'/></a>
    <form>
    <input type='submit' name ='save' value='save' class ='save' title ='Save your contribution' onClick='alert("Thank you for contributting!")'/>
    </form>
    <a href = 'teatimenarratives.cgi' class ='narratives'></a>
    <div class = 'teatime'>
"""

openlist = []
openlista = []
possible = []
trespossible = []

def cleanup(name,list):
    boole = True
    for line in lines:
        if boole:
            miau = urllib2.urlopen(line).read()
            parse = html5lib.parse(miau, namespaceHTMLElements=False)
            path = urlparse(line).path.strip("/")
            listed = path.split("/")
            author = listed[0]
            mimimi = parse.findall(".//div")
            for mimi in mimimi:
                if mimi.attrib.get("id") == "chaptext":
                    for search in mimi:
                        if search.text != None:
                            searchme = search.text
                            if name in searchme:
                                boole = False
                                yup = searchme.split(".")
                                for y in yup:
                                    if name in y:
                                        list.append(y)
    talk = author + ": " + random.choice(list).encode("utf-8").replace('"'," ") + "."
    talk = talk.replace("\n"," ") 
    talk = talk.replace("Mr.","Cavendish.") 
    return talk.replace("Mrs.","Chansey.")
def select(frm,lista):
    que = frm.strip().split()
    for rp in que:
        if len(rp) >= 4:
            lista.append(rp)                                          

if q:
    select(q,trespossible)
    nm = random.choice(trespossible)
    talk = cleanup(nm,openlist)
    print """<p>"""+talk+"""</p>"""
    belezaFinal.write(talk + "\n")

beleza = open("TeaTimeTalk/"+sessao+".txt")
pois = beleza.readlines()
x = 0
for line in pois:
    if x%2:
        print """<p class='reply'>you: """+line+"""</p>"""
    else:
        print """<p>"""+line+"""</p>"""
    x = x + 1

if reply:
    select(reply,possible)
    rs = random.choice(possible)
    try:
        merci = cleanup(rs,openlista)
        print """<p class ='reply'>you: """+reply+"""</p>"""
        print """<p>"""+merci+"""</p>"""
        belezaFinal.write(reply+"\n")
        belezaFinal.write(merci+"\n")
    except IndexError:
        print """<p>I'm sorry, we cannot find anything...</p>"""


if save:
    armazem.write("\n")
    for line in pois:
        if ":" in line:
            ls=line.split(":")
            armazem.write(ls[1].lstrip(" "))
        else:
            armazem.write(line)
    armazem.write("\n.........................................................................................\n")

print """
<form>
<input type='text' name ='reply' id='reply' size='77' placeholder='Reply...' />    
<input type='submit' value='Send!' /> <br>
</form>
</div>
"""  
print """</body>
</html>"""
