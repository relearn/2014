Create a git publishing pipeline
================================

Based on: http://joemaller.com/990/a-web-focused-git-workflow/

TODO: document the setup on the relearn server.


1. Clone the (bare) repository into your webserver directory:

    cd /var/www
    git clone /home/relearn/git/new_babylon.git/

2. Add a post-update hook on the bare repository to automatically pull (actually it fetches and hard resets to make sure it ALWAYS works) when updated:

    cd
    cd git/new_babylon.git/hooks
    cp post-update.sample post-update

    # mostly next step isn't necessary as it's already +x
    chmod +x post-update

    nano post-update

and edit this file to be:

    #!/bin/sh
    #
    # An example hook script to prepare a packed repository for use over
    # dumb transports.
    #
    # To enable this hook, rename this file to "post-update".

    cd /path/to/www/repo || exit
    unset GIT_DIR
    git fetch --all
    git reset --hard origin/master
    exec git update-server-info

